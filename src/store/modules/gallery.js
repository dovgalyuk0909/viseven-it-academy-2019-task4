export default {
    state: {
        currentItem: null,
        popupIs: false,
        paternStyleGallery: [
            {photo: {id : 0, gridAreaStyle : [1,1,1,3]}},
            {photo: {id : 4, gridAreaStyle : [3,2,3,4]}},
            {photo: {id : 5, gridAreaStyle : [1,3,3,3]}}
        ],
        photoStyleSet: {everyNingthPhotoCounter : 9, gridAreaSum : 4},
        gallery: [
            { src: 'Layer1.jpg',id:0, like: 12, dislike: 2, coments: [{ name: 'noname09', msg: 'Hello viseven', date: 'today',hours:'4:20 AM'}] },
            { src: 'Layer2.jpg',id:1, like: 7, dislike: 9, coments: [{ name: 'olegas', msg: 'Hello viseven', date: 'today',hours:'5:12 AM'}] },
            { src: 'Layer3.jpg',id:2, like: 5, dislike: 7, coments: [{ name: 'masha07', msg: 'Hello viseven', date: 'today',hours:'7:10 PM'}] },
            { src: 'Layer4.jpg',id:3, like: 3, dislike: 6, coments: [{ name: 'leonid00', msg: 'Hello viseven', date: 'today',hours:'11:02 PM'}] },
            { src: 'Layer7.jpg',id:4, like: 6, dislike: 21, coments: [{ name: 'zhuk', msg: 'Hello viseven', date: 'today',hours:'4:12 PM'}] },
            { src: 'Layer5.jpg',id:5, like: 8, dislike: 12, coments: [{ name: 'jsprog', msg: 'Hello viseven', date: 'today',hours:'10:09 AM'}] },
            { src: 'Layer6.jpg',id:6, like: 1, dislike: 3, coments: [{ name: 'vitek009', msg: 'Hello viseven', date: 'today',hours:'3:22 PM'}] },
            { src: 'pic.png',id:7, like: 6, dislike: 1, coments: [{ name: 'sanek777', msg: 'Hello viseven', date: 'today',hours:'6:02 PM'}] }
        ],
        storage: {
            items: JSON.parse(localStorage.getItem('items'))
		}
    },
    mutations: {
        createGallery(state) {
            if(window.localStorage.items) {
                return
            } else {
                state.storage.items = state.gallery;
            }
        },
        updateDate(state) {
            const date = new Date();
            state.currentDate = date.toDateString();
        },
        
        setCurrentItem(state,newItem) {
            state.currentItem = newItem;
            state.popupIs = !state.popupIs;
        },
        
        addToStorage(state,value) {
            state.storage.items = [...state.storage.items, value];
            localStorage.setItem('items', JSON.stringify(state.storage.items));
        },
        popupOperation(state,str) {
            if(state.currentItem.hasOwnProperty('check') === false) {
			    state.currentItem[str] += 1;			
            } else if (state.currentItem.check === 'clickedlike' && str === 'dislike') {
			    state.currentItem.like -= 1;
			    state.currentItem.dislike += 1;
		    } else if (state.currentItem.check === 'clickeddislike' && str === 'like') {
			    state.currentItem.dislike -= 1;
			    state.currentItem.like += 1;
		    }
            state.currentItem.check = `clicked${str}`;
        },
        addComment(state, comment) {
            state.currentItem.coments.push(comment)
        },
        updateStorage(state) {
            const array = [...state.storage.items];
            array.splice(state.currentItem.id, 1, state.currentItem);
            state.storage.items = array;
            localStorage.setItem('items', JSON.stringify(state.storage.items));
        },
    },
    actions: {
        addImage( {commit}, galleryLength ) {
            let getImagePath = prompt('Enter name of photos from the folder "@/assets/img/"(for example: img5)');
            let path = getImagePath + '.jpg';
            if(getImagePath === null){ return }
            const newItem = {
                src: path,
                id: galleryLength,
                like: 0,
                dislike: 0, 
                coments: []
            };
            commit('addToStorage',newItem);
        },
        operation({ commit },str) {
            commit('popupOperation', str)
            commit('updateStorage')
        },
        operationAdd({ commit },comment) {
            const date = new Date();
            const newComment = {
		        name: comment.name,
		        msg: comment.text,
		        date: date.getUTCDate(),
		        hours: date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
            }
            commit('addComment', newComment)
            commit('updateStorage')
        }
    },
    getters: {
        getPopupIs(state) {
            return state.popupIs;
        },
        getImageUrl(state) {
            return require ('@/assets/img/' + state.currentItem.src);
        },
        getCurrentItem(state) {
            return state.currentItem;
        },
        getGallery(state) {
            return state.storage.items;
        },
        getStyleGallery(state) {
            return state.paternStyleGallery
        },
        getPhotoStyleSet(state) {
            return state.photoStyleSet
        }
    }
}