import home from '@/components/gallery'

import VueRouter from 'vue-router'

export default new VueRouter({
    routes: [
        {
            path: '',
            component: home
        }
    ],
    mode: 'history'
})